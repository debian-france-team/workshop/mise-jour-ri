Travail sur la mise à jour du règlement intérieur
=================================================

- [x] Modalités de remboursement Remboursement
- [x] Ajouter vote du 11 Juillet 2019 - La somme de 40€ maximum par an peut donc être accordée sous forme de goodies [...] 
- [x] Ajouter vote du 02 Juillet 2019 - Poste de « Community Manager » et compte sur les réseaux sociaux
- [X] Éditer URL du site web (pour clôner), une fois migré sur Salsa
- [x] Revoir délégation des pouvoir pour adapter avec le poste des « vice »
- [ ] Transférer sur Salsa le dépôt PV-AG après avoir retiré les signature
- [ ] Modifier l'URL « /vote » du RI (voir 2.3 Phase de vote d'une AG électronique)
- [ ] Revoir délégation des pouvoirs (ART 12) pour ne pas citer les délégations, mais faire référence à un vote annuel du CA
